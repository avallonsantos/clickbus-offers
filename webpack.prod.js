// Importing required dependencies

const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlTemplateConfig = require('./config/template');

// Defining S3 landing page URL

const PROD_S3_LANDING_PAGE_URL = 'https://static.clickbus.com.br/landing-pages/';

// Config to display CMS placeholders

HtmlTemplateConfig.renderPlaceholders = true;

// Webpack Production configuration

module.exports = merge(common, {
    mode: 'production',
    output: {
        publicPath: ''
    },
    plugins: [
        new HtmlWebpackPlugin(HtmlTemplateConfig)
    ]
});
