/**
 * Importing dependencies
 */
import formatCurrency from './helpers/format-currency';

class OffersTable {
    constructor(target, routes) {
        this.target = document.querySelector(target);
        this.routes = routes;
    }

    changeFinalUrlDate() {
        $('#table-offers select').on('change', function () {
            let date = $(this).val();
            let link = $(this).parent().parent().next('td').find('a').attr('href').split('=')[0] + '=' + date;
            $(this).parent().parent().next('td').find('a').attr('href', link)
        });
    };

    filterTable() {
        let triggerFilter = $('.filter-trigger'), rows = this.target.querySelectorAll('.row-offer');

        triggerFilter.on('change', function () {
            let triggerOriginValue = $('#origin-offer').val();
            let triggerDestinationValue = $('#destination-offer').val();
            let trigger = $(this);

            if(triggerOriginValue !== '' && triggerDestinationValue === '') {
                return rows.forEach(row => {
                    if (triggerOriginValue === row.getAttribute('data-origin-offer')) {
                        return row.style.display = 'table-row';
                    }
                    return row.style.display = 'none';
                })
            }

            if(triggerOriginValue === '' && triggerDestinationValue !== '') {
                return rows.forEach(row => {
                    if (triggerDestinationValue === row.getAttribute('data-destination-offer')) {
                        return row.style.display = 'table-row';
                    }
                    return row.style.display = 'none';
                })
            }

            if(triggerOriginValue !== '' && triggerDestinationValue !== '') {
                return rows.forEach(row => {
                    if (triggerOriginValue === row.getAttribute('data-origin-offer') && triggerDestinationValue === row.getAttribute('data-destination-offer')) {
                        return row.style.display = 'table-row';
                    }
                    return row.style.display = 'none';
                })
            }

            rows.forEach(row => row.style.display = 'table-row');
        });
    }

    createTable() {
        const insertedOrigins = [];
        const insertedDestinations = [];
        this.routes.forEach(route => {
            let availables = route.availableDates.split(',');
            let row = `<tr class="row-offer" data-origin-offer="${route.originSlug}" data-destination-offer="${route.destinationSlug}">
                   <td>${route.origin}</td>
                   <td>${route.destination}</td>
                   <td>${route.type}</td>
                   <td>
                        <select class="available-dates">
                            ${availables.map(data => {
                let dateSplited = data.trim().split('-');
                return `<option value="${data.trim()}">${dateSplited[2]}/${dateSplited[1]}/${dateSplited[0]}</option>`
            }).join('')}
                        </select>
                    </td>
                   <td class="price-td">
                        <a target="_blank"
                           href="https://www.clickbus.com.br/onibus/${route.originSlug}/${route.destinationSlug}?departureDate=${availables[0]}">
                            <strike>${formatCurrency(parseFloat(route.referencePrice))}</strike>
                            <span>${formatCurrency(parseFloat(route.price))}</span>
                            <i class="icon-search"></i>
                        </a>
                    </td>
               </tr>`;

            if (!insertedDestinations.includes(route.destinationSlug)) {
                document.getElementById('destination-offer').insertAdjacentHTML('beforeend', `<option value="${route.destinationSlug}">${route.destination}</option>`);
            }

            if (!insertedOrigins.includes(route.originSlug)) {
                document.getElementById('origin-offer').insertAdjacentHTML('beforeend', `<option value="${route.originSlug}">${route.origin}</option>`);
            }

            insertedOrigins.push(route.originSlug);
            insertedDestinations.push(route.destinationSlug);

            this.target.querySelector('tbody').insertAdjacentHTML('beforeend', row);
        });

        this.changeFinalUrlDate();
        this.filterTable();
    }
}

export default OffersTable;