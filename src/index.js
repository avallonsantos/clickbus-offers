// Importing global sass

import './assets/scss/app.scss';

/**
 * Importing route offers
 */

import routes from './routes.csv';

/**
 * Importing Create Offers Table function
 */
import OffersTable from "./components/table-offers";

/**
 * Importing Create Accordions function
 */

import CreateAccordions from "./components/accordion";

document.addEventListener('DOMContentLoaded', () => {
    /**
     * Creating offers table
     * @type {OffersTable}
     */
    const Offers = new OffersTable('#table-offers', routes);
    Offers.createTable();

    /**
     * Creating accordiins
     */

    CreateAccordions();
});