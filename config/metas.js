// Array above contains all meta tags that will be rendered in final LP code
// To use it just create a new object inside array with a couple of attributes. Generally name and content

module.exports = [
    {
        name: 'og:url',
        content: 'Viagem Corpus Christi: Passagens Baratas | ClickBus'
    },

    {
        name: 'description',
        content: 'Viagem Corpus Christi: Passagens Baratas | ClickBus'
    },
]